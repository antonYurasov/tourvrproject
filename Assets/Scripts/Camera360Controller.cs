﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Controlling the position of the camera by mouse
/// </summary>
public class Camera360Controller: MonoBehaviour
{



    [SerializeField]
    private float Sensitivity;
    [SerializeField]
    private float InertionDemp;
    [SerializeField]
    private float ThresholdXAngle;

    private Camera cameraView;

    private float previousXDisplacementValue = 0;
    private float previousYDisplacementValue = 0;

    private float counter = 0;

    private float currentXDisplacement = 0;
    private float currentYDisplacement = 0;


    private void Awake()
    {
        cameraView = this.gameObject.GetComponent<Camera>();

        if (cameraView == null)
        {
            cameraView = this.gameObject.AddComponent<Camera>();
        }

        this.gameObject.name = "MainCamera";
        this.gameObject.tag = "MainCamera";

        Sensitivity = 2;
        InertionDemp = 3;
        ThresholdXAngle = 85;
    }


    private void Update()
    {        

        float inputMouseXDisplacement = Input.GetAxis("Mouse X")*Sensitivity;

        float inputMouseYDisplacement = Input.GetAxis("Mouse Y")*Sensitivity;
             

        if(Input.GetMouseButton(0))
        {
            counter = 0;

            currentXDisplacement = inputMouseXDisplacement;
            currentYDisplacement = inputMouseYDisplacement;
            
            previousXDisplacementValue = currentXDisplacement;
            previousYDisplacementValue = currentYDisplacement;

        }
        else
        {
            if (TryLeadValueToOne(ref counter, InertionDemp * Time.deltaTime))
            {
                currentXDisplacement = GetLerpedValue(previousXDisplacementValue, 0, counter);
                currentYDisplacement = GetLerpedValue(previousYDisplacementValue, 0, counter);
            }
        }

        Vector3 rotation = new Vector3(currentYDisplacement, -currentXDisplacement, 0);

        Quaternion rot = FollowCursorRotation(this.transform.localRotation, rotation);

        Vector3 correction = new Vector3(0, 0, -rot.eulerAngles.z);

        rot = CorrectRotation(rot, correction);

        this.transform.localRotation = rot;    
        
    }
    
    /// <summary>
    /// Increse value by step while it less than one
    /// </summary>
    /// <param name="value">Reference to value</param>
    /// <param name="step">Increasing step</param>
    /// <returns>True if the value less than one</returns>
    private bool TryLeadValueToOne(ref float value, float step)
    {
        if(value<1)
        {
            value += step;
        }
        else
        {
            return false;
        }

        return true;
    }
    
    /// <summary>
    /// Get lerped value
    /// </summary>
    /// <param name="startValue">Current value</param>
    /// <param name="endValue">Target value</param>
    /// <param name="t">Lerp parameter</param>
    /// <returns>Lerped value</returns>
    private float GetLerpedValue(float startValue, float endValue, float t)
    {
        return Mathf.Lerp(startValue, endValue, t);
    }

    /// <summary>
    /// Rotate previousRotation by eulerRotation vector
    /// </summary>
    /// <param name="previousRotation">Previous rotation of the object</param>
    /// <param name="eulerRotation">Rotation vector with angles</param>
    /// <returns>Previous rotation rotated by vector</returns>
    private Quaternion FollowCursorRotation(Quaternion previousRotation, Vector3 eulerRotation)
    {        

        previousRotation *= Quaternion.AngleAxis(eulerRotation.y, this.transform.InverseTransformDirection(Vector3.up)) * Quaternion.AngleAxis(eulerRotation.x, Vector3.right);
        
        previousRotation = Quaternion.Euler(new Vector3(ClampAngle(previousRotation.eulerAngles.x), previousRotation.eulerAngles.y, previousRotation.eulerAngles.z));

        return previousRotation;
    }


    /// <summary>
    /// Clamp angle between thresholds
    /// </summary>
    /// <param name="angle">Current angle</param>
    /// <returns>Clamped angle</returns>
    private float ClampAngle(float angle)
    {
        float maxThreshold = 360;
        float minThreshold = 0;

        if(angle>0 && angle<180)
        {
            maxThreshold = ThresholdXAngle;
        }
        else if(angle>180 && angle<360)
        {
            minThreshold = 360 - ThresholdXAngle;
        }

        return Mathf.Clamp(angle, minThreshold, maxThreshold);
    }

    /// <summary>
    /// Add correction vector to rotation in euler angles
    /// </summary>
    /// <param name="initalRotation">Initial rotation of the object</param>
    /// <param name="correction">Correction vector</param>
    /// <returns>Corrected rotation</returns>
    private Quaternion CorrectRotation(Quaternion initalRotation, Vector3 correction)
    {
        Vector3 initialRotationInEuler = initalRotation.eulerAngles;

        return Quaternion.Euler(initialRotationInEuler + correction);
    }

    
}
