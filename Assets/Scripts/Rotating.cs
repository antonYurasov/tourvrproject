﻿using UnityEngine;

public class Rotating : MonoBehaviour
{

    public float XAngle;
    public float YAngle;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.A))
        {
            this.transform.localRotation *= Quaternion.AngleAxis(YAngle, this.transform.InverseTransformDirection(Vector3.up));            
        }


        if(Input.GetKeyDown(KeyCode.B))
        {
            this.transform.localRotation *= Quaternion.AngleAxis(XAngle, Vector3.right);
        }
    }

}
